const HtmlWebpackPlugin = require("html-webpack-plugin");
const ReactRefreshPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

const path = require("path");

const prod = process.env.NODE_ENV === "production";

module.exports = {
	mode: prod ? "production" : "development",
	entry: "./src/app/index.tsx",
	output: {
		path: __dirname + "/dist/",
		publicPath: "/",
	},
	devServer: {
		hot: true,
		port: 4200,
		historyApiFallback: true,
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				include: path.join(__dirname, "src"),
				use: "babel-loader",
			},
		],
	},
	plugins: [
		!prod && new ReactRefreshPlugin(),
		new ForkTsCheckerWebpackPlugin(),
		new HtmlWebpackPlugin({
			filename: "./index.html",
			template: "./public/index.html",
		}),
	].filter(Boolean),
	resolve: {
		alias: {
			"@app": path.resolve(__dirname, "src/app/"),
			"@entities": path.resolve(__dirname, "src/entities/"),
			"@features": path.resolve(__dirname, "src/features/"),
			"@pages": path.resolve(__dirname, "src/pages/"),
			"@shared": path.resolve(__dirname, "src/shared/"),
		},
		extensions: [".tsx", ".ts", ".js"],
	},
	devtool: prod ? undefined : "source-map",
};
