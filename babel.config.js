module.exports = (api) => {
	// This caches the Babel config
	api.cache.using(() => process.env.NODE_ENV);
	return {
		presets: [
			"@babel/preset-env",
			"@babel/preset-typescript",
			// Enable development transform of React with new automatic runtime
			[
				"@babel/preset-react",
				{ development: !api.env("production"), runtime: "automatic" },
			],
		],
		// react refresh on dev only TODO
		plugins: [
			!api.env("production") && "react-refresh/babel",
			"@babel/plugin-transform-runtime",
		].filter(Boolean),
	};
};
