import { RouteObject, useRoutes } from "react-router";
import { Typography } from "@mui/material";

import { MainPage, ProductsPage, AuthPageConnector, ProfilePage } from "@pages";
import { accessTokenSelector } from "@features/userAuth";
import { useAppSelector } from "@shared/hooks";

export const Routes = () => {
	const accessToken = useAppSelector(accessTokenSelector);

	const routes: RouteObject[] = [];

	if (!!accessToken) {
		routes.push(
			{
				path: "/",
				element: <MainPage />,
			},
			{
				path: "products",
				children: [
					{
						index: true,
						element: <ProductsPage />,
					},
					{
						path: ":id",
						element: <Typography>Product</Typography>,
					},
				],
			},
			{
				path: "profile",
				element: <ProfilePage />,
			},
		);
	} else {
		routes.push({
			path: "/",
			element: <AuthPageConnector />,
		});
	}

	//Make page for 404
	routes.push({
		path: "*",
		element: <div>404</div>,
	});

	const routesNodes = useRoutes(routes);

	return routesNodes;
};
