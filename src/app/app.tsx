import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { ThemeProvider, styled } from "@mui/material";

import { Routes } from "@entities/routing";
import { theme } from "@shared/ui/theme";
import { store, persistor } from "@entities/store/";

const AppWrapper = styled("div")({
	display: "flex",
	flexDirection: "column",
	flex: 1,
	width: "100%",
	height: "100%",
});

const App = () => {
	return (
		<AppWrapper>
			<Provider store={store}>
				<PersistGate loading={null} persistor={persistor}>
					<ThemeProvider theme={theme}>
						<Router>
							<Routes />
						</Router>
					</ThemeProvider>
				</PersistGate>
			</Provider>
		</AppWrapper>
	);
};

export default App;
