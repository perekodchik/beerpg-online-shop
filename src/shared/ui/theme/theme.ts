import { createTheme } from "@mui/material";

export const theme = createTheme({
	palette: {
		primary: {
			main: "#ff5722",
			light: "#FF784E",
			dark: "#f4511e",
			contrastText: "#FFFFFF",
		},
		secondary: {
			main: "#1976d2",
			light: "#4791DB",
			dark: "#115293",
		},
	},
});
