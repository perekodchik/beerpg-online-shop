import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { RootState } from "@entities/store/store";

type AuthState = {
	loading: boolean;
	accessToken: string;
};

const initialState: AuthState = {
	loading: false,
	accessToken: "",
};

export const sendAuthData = createAsyncThunk(
	"userAuth/sendAuthData",
	async ({ login, password }: { login: string; password: string }) => {
		//Here should be API request to check user data
		await new Promise((resolve) => setTimeout(resolve, 1000));
		return { login, password, accessToken: "aslkdj1290jdklw" };
	},
);

export const userAuth = createSlice({
	name: "userAuth",
	initialState,
	reducers: {
		logout: (state) => {
			state.accessToken = "";
		},
	},
	extraReducers(builder) {
		builder
			.addCase(sendAuthData.pending, (state, action) => {
				state.loading = true;
			})
			.addCase(sendAuthData.fulfilled, (state, action) => {
				state.loading = false;
				state.accessToken = action.payload.accessToken;
			})
			.addCase(sendAuthData.rejected, (state, action) => {
				state.loading = false;
			});
	},
});

export const { logout } = userAuth.actions;

export const accessTokenSelector = (state: RootState) =>
	state.userAuth.accessToken;

export const userAuthReducer = userAuth.reducer;
