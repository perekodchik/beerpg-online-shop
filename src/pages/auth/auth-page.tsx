import React from "react";
import { Typography, styled, TextField, Button } from "@mui/material";
import { useAppDispatch } from "@shared/hooks";
import { sendAuthData } from "@features/userAuth/userAuth";
import { useForm } from "react-hook-form";

export type AuthPageProps = {
	onSubmit: () => void;
};

type TAuthForm = {
	login: string;
	password: string;
};

const AuthPageWrapper = styled("div")(({ theme }) => ({
	display: "flex",
	flex: 1,
	flexDirection: "column",
	alignItems: "center",
	padding: theme.spacing(1),
}));

const FormWrapper = styled("form")(({ theme }) => ({
	display: "flex",
	flex: 1,
	width: "100%",
	gap: theme.spacing(1),
	flexDirection: "column",
}));

const Spacer = styled("div")({ flex: 1 });

export const AuthPage: React.FC<AuthPageProps> = ({ onSubmit }) => {
	const dispatch = useAppDispatch();

	const { register, handleSubmit } = useForm<TAuthForm>();

	return (
		<AuthPageWrapper>
			<Typography color="primary" variant="h1">
				Auth page
			</Typography>
			<FormWrapper>
				<TextField placeholder="login" {...register("login")} />
				<TextField placeholder="password" {...register("password")} />
				<Spacer />
				<Button
					onClick={handleSubmit((data) => {
						dispatch(
							sendAuthData({
								login: data.login,
								password: data.password,
							}),
						);
						console.log(data);
					})}
					variant="contained"
				>
					LOGIN
				</Button>
			</FormWrapper>
		</AuthPageWrapper>
	);
};
