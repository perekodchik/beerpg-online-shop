import React from "react";
import { Typography, styled, Button } from "@mui/material";

const ProfilePageWrapper = styled("div")({
	display: "flex",
	flex: 1,
	flexDirection: "column",
});

export const ProfilePage = () => {
	return (
		<ProfilePageWrapper>
			<Typography variant="h1" color="primary">
				Profile page
			</Typography>
		</ProfilePageWrapper>
	);
};
