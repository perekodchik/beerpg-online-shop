import React from "react";

import { Typography } from "@mui/material";

export const MainPage: React.FC = () => {
	return (
		<Typography color="primary" variant="h1">
			Main page
		</Typography>
	);
};
