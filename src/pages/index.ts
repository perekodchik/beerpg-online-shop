export { MainPage } from "./main";
export { ProductsPage } from "./products/";
export { AuthPageConnector } from "./auth";
export { ProfilePage } from "./profile";
