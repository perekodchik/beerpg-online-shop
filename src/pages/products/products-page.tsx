import React from "react";
import { Typography, styled, Button } from "@mui/material";

const ProductsPageWrapper = styled("div")({
	display: "flex",
	flex: 1,
	flexDirection: "column",
});

export const ProductsPage = () => {
	return (
		<ProductsPageWrapper>
			<Typography variant="h2">Products page</Typography>
			<Button variant="contained">Press me</Button>
		</ProductsPageWrapper>
	);
};
